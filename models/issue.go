// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package models

import "time"

// Issue is a pull request or an issue from the Gitea instance
type Issue struct {
	ID     int64 `xorm:"pk"`
	RepoID int64 `xorm:"index"`

	Excluded bool `xorm:"index"`
	Stale    bool `xorm:"index"`
	Closed   bool `xorm:"index"`

	// IndexedAt represents the time the issue was indexed last time
	IndexedAt *time.Time `xorm:"updated index"`
	// UpdatedAt represents the time when the issue was updated last
	UpdatedAt *time.Time
	// MarkedAt represents the time when the issue was marked as stale
	MarkedAt *time.Time
}
