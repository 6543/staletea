// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package models

import "github.com/go-xorm/xorm"

// Repository represents a Gitea repository indexed at the local database
type Repository struct {
	ID     int64  `xorm:"pk"`
	UserID int64  `xorm:"index"`
	Owner  string `xorm:"unique(owner_name)"`
	Name   string `xorm:"unique(owner_name)"`

	Activated bool
}

// FindRepositoriesByUserID returns all repos of an user
func FindRepositoriesByUserID(userID int64) ([]*Repository, error) {
	return findRepositoriesByUserID(x, userID)
}

func findRepositoriesByUserID(e *xorm.Engine, userID int64) ([]*Repository, error) {
	repos := make([]*Repository, 0)
	return repos, e.Where("user_id = ?", userID).Find(&repos)
}
