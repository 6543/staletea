// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitea.com/jonasfranz/staletea/web"
	"github.com/urfave/cli"
)

// CmdRun is the command to start the webserver
var CmdRun = cli.Command{
	Action: run,
	Name:   "run",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "address",
			Value: ":3030",
		},
	},
}

func run(ctx *cli.Context) error {
	return web.StartServer(ctx.String("address"))
}
