module gitea.com/jonasfranz/staletea

go 1.12

require (
	code.gitea.io/sdk v0.0.0-20190602153954-7e711e06b588 // indirect
	code.gitea.io/sdk/gitea v0.0.0-20190602153954-7e711e06b588
	github.com/gin-contrib/sessions v0.0.0-20190512062852-3cb4c4f2d615
	github.com/gin-gonic/gin v1.4.0
	github.com/go-xorm/core v0.6.0
	github.com/go-xorm/xorm v0.7.1
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/spf13/viper v1.4.0
	github.com/urfave/cli v1.20.0
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5 // indirect
	golang.org/x/lint v0.0.0-20190409202823-959b441ac422 // indirect
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65 // indirect
	golang.org/x/oauth2 v0.0.0-20190523182746-aaccbc9213b0
	golang.org/x/sys v0.0.0-20190602015325-4c4f7f33c9ed // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190603231351-8aaa1484dc10 // indirect
)
