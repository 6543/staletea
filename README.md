# StaleTea

[![Build Status](https://drone.gitea.com/api/badges/jonasfranz/staletea/status.svg)](https://drone.gitea.com/jonasfranz/staletea)
[![Go Report Card](https://goreportcard.com/badge/gitea.com/jonasfranz/staletea)](https://goreportcard.com/report/gitea.com/jonasfranz/staletea)
[![Documentation](https://godoc.org/gitea.com/jonasfranz/staletea?status.svg)](http://godoc.org/gitea.com/jonasfranz/staletea)

StaleTea is a simple stalebot for Gitea.

## Example config.yml

```yaml
closecomment: This issue was closed automatically since it was marked as stale and
  had no recent activity.
daysuntilclose: 7
daysuntilstale: 60
exemptlabels: []
host: http://localhost:3030
markcomment: |-
  This issue has been automatically marked as stale because it has not had
    recent activity. It will be closed in %d days if no further activity occurs. Thank you
    for your contributions.
only: []
onlylabels: []
stalelabel: wontfix
unmarkcomment: ""

gitea:
  client_id: "YOUR_CLIENT_ID"
  client_secret: "YOUR_CLIENT_SECRET"
  url: https://gitea.com

session_secret: YOUR_SECRET
```