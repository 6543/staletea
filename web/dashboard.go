// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package web

import (
	"fmt"
	"gitea.com/jonasfranz/staletea/config"
	"gitea.com/jonasfranz/staletea/models"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
)

func showDashboard(ctx *gin.Context) {
	session := sessions.Default(ctx)
	user, ok := session.Get("user").(*models.User)
	if !ok || user == nil {
		ctx.Redirect(http.StatusTemporaryRedirect, fmt.Sprintf("%s/login", config.BaseURL.Get().(string)))
		return
	}
	activatedRepos, err := models.FindRepositoriesByUserID(user.ID)
	if err != nil {
		_ = ctx.AbortWithError(500, err)
		return
	}
	remoteRepos, err := user.GiteaClient().ListMyRepos()
	if err != nil {
		_ = ctx.AbortWithError(500, err)
		return
	}
	combinedRepos := make(map[int64]*models.Repository, len(activatedRepos))

	for _, repo := range activatedRepos {
		combinedRepos[repo.ID] = repo
	}

	for _, repo := range remoteRepos {
		combinedRepos[repo.ID] = &models.Repository{
			ID:        repo.ID,
			UserID:    user.ID,
			Owner:     repo.Owner.UserName,
			Name:      repo.Name,
			Activated: false,
		}
	}

	data := map[string]interface{}{
		"repos": combinedRepos,
		"user":  user,
	}

	ctx.HTML(200, "dashboard.tmpl", data)
}

func handleActivate(ctx *gin.Context) {

}
